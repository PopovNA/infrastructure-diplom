## Создание облачной инфраструктуры 
![Репозиторий с конфигурационными файлами Terraform](https://gitlab.com/PopovNA/infrastructure-diplom/-/tree/main/terraform)
## Создание Kubernetes кластера
Создан Kubernetes кластер с размещение нод в 3 разных подсетях
![](./images/Screenshot%20from%202022-06-28%2018-18-30.jpg)

Подключение к кластеру
```
nikolay@nikolay-VirtualBox:~/yandex-diplom$ yc managed-kubernetes cluster get-credentials --id catuv98hfauilgjln84c --external --force
```
```
nikolay@nikolay-VirtualBox:~/yandex-diplom$ kubectl get pods --all-namespaces
NAMESPACE     NAME                                     READY   STATUS    RESTARTS   AGE
kube-system   coredns-5f8dbbff8f-ddx6n                 1/1     Running   0          38m
kube-system   coredns-5f8dbbff8f-gxx2p                 1/1     Running   0          34m
kube-system   ip-masq-agent-bg4vq                      1/1     Running   0          34m
kube-system   ip-masq-agent-t4s5k                      1/1     Running   0          34m
kube-system   ip-masq-agent-w5tmd                      1/1     Running   0          35m
kube-system   kube-dns-autoscaler-598db8ff9c-tz69p     1/1     Running   0          38m
kube-system   kube-proxy-pwrgs                         1/1     Running   0          35m
kube-system   kube-proxy-v2b8k                         1/1     Running   0          34m
kube-system   kube-proxy-z9kcv                         1/1     Running   0          34m
kube-system   metrics-server-v0.3.1-6b998b66d6-cxsxq   2/2     Running   0          34m
kube-system   npd-v0.8.0-4tmp9                         1/1     Running   0          34m
kube-system   npd-v0.8.0-5m7v4                         1/1     Running   0          34m
kube-system   npd-v0.8.0-ljsrr                         1/1     Running   0          35m
kube-system   yc-disk-csi-node-v2-46qkb                6/6     Running   0          34m
kube-system   yc-disk-csi-node-v2-7q5ft                6/6     Running   0          35m
kube-system   yc-disk-csi-node-v2-xsh6j                6/6     Running   0          34m
```
## Создание тестового приложения
![Git репозиторий с тестовым приложением и Dockerfile](https://gitlab.com/PopovNA/diplom_netology)  
![Регистр с собранным docker image.](https://hub.docker.com/repository/docker/popovna/nginx)  

## Подготовка cистемы мониторинга и деплой приложения
Воспользолся пакетом kube-prometheus, который уже включает в себя Kubernetes оператор для grafana, prometheus, alertmanager и node_exporter.  
![Файлы  конфигурации](https://gitlab.com/PopovNA/infrastructure-diplom/-/tree/main/my-kube-prometheus)

```
kubectl apply --server-side -f manifests/setup
until kubectl get servicemonitors --all-namespaces ; do date; sleep 1; echo ""; done
kubectl apply -f manifests/
```
```
nikolay@nikolay-VirtualBox:~/yandex-diplom/my-kube-prometheus$ kubectl get pod -n monitoring
NAME                                   READY   STATUS    RESTARTS   AGE
alertmanager-main-0                    2/2     Running   0          3m49s
alertmanager-main-1                    2/2     Running   0          3m49s
alertmanager-main-2                    2/2     Running   0          3m49s
blackbox-exporter-5c545d55d6-s47v8     3/3     Running   0          3m49s
grafana-7d6799b6b-79zlx                1/1     Running   0          3m40s
kube-state-metrics-54bd6b479c-bfbzz    3/3     Running   0          3m39s
node-exporter-5dq79                    2/2     Running   0          3m37s
node-exporter-65l4n                    2/2     Running   0          3m37s
node-exporter-l52pf                    2/2     Running   0          68s
node-exporter-vk9tv                    2/2     Running   0          3m37s
prometheus-adapter-7bf7ff5b67-7czkz    1/1     Running   0          3m34s
prometheus-adapter-7bf7ff5b67-brfc2    1/1     Running   0          3m34s
prometheus-k8s-0                       2/2     Running   0          3m31s
prometheus-k8s-1                       2/2     Running   0          3m31s
prometheus-operator-54dd69bbf6-5fdx4   2/2     Running   0          4m12s
```

Деплой приложения:
```
nikolay@nikolay-VirtualBox:~/yandex-diplom/app_kube$ kubectl get pod,deployment,svc,ingress -n my-nginx
NAME                           READY   STATUS    RESTARTS   AGE
pod/my-nginx-bb47cd44b-tzqlz   1/1     Running   0          4m23s

NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/my-nginx   1/1     1            1           4m23s

NAME               TYPE           CLUSTER-IP      EXTERNAL-IP       PORT(S)        AGE
service/my-nginx   LoadBalancer   10.96.247.172   178.154.192.124   80:32104/TCP   4m24s

NAME                                      CLASS    HOSTS             ADDRESS   PORTS   AGE
ingress.networking.k8s.io/ingress-nginx   <none>   diplom-popov.ru             80      4m24s
```
Проверка доступа приложения 
![image](./images/Screenshot from 2022-06-28 18-19-58.jpg)
ip 178.154.192.124
Проверка доступа grafana
![image](./images/Screenshot from 2022-06-28 19-17-39.png)
ip http://51.250.44.53:3000

для проверки: 
user: Auditor
password: Auditor

## Установка и настройка CI/CD
В качестве системы CI/CD была использована gitlab ci.  
![Git репозиторий] (https://gitlab.com/PopovNA/diplom_netology)  
Настроен ![pipeline](https://gitlab.com/PopovNA/diplom_netology/-/blob/main/.gitlab-ci.yml)  
Для подключения к кластеру kubernetes настроен ![агент](https://gitlab.com/PopovNA/diplom_netology/-/tree/main/.gitlab/agents/agent-netology)  
Установка агента в кластере:
```
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install agent-netology gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set image.tag=v15.1.0 \
    --set config.token=$GITLAB_AGENT_TOKEN \
    --set config.kasAddress=wss://kas.gitlab.com
```
