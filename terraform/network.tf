resource "yandex_vpc_network" "vpc" {
  name = "network"
}

resource "yandex_vpc_subnet" "public" {
  for_each = var.public_subnet
  name = "public-${each.key}"
  zone           = "${each.key}"
  network_id     = "${yandex_vpc_network.vpc.id}"
  v4_cidr_blocks = "${each.value}"
}

resource "yandex_vpc_subnet" "private" {
  for_each = var.private_subnet
  name = "private-${each.key}"
  zone           = "${each.key}"
  network_id     = "${yandex_vpc_network.vpc.id}"
  v4_cidr_blocks = "${each.value}"
}
