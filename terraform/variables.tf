variable "ya_token" {
  type = string
  description = "Yandex Cloud security OAuth token"
}

variable "ya_cloud_id" {
  type = string
  description = "Yandex Cloud ID where resources will be created"
}

variable "ya_folder_id" {
  type = string
  description = "Yandex Cloud Folder ID where resources will be created"
}

variable "public_subnet"{
 type = map
 default = {
   "ru-central1-a" = ["192.168.10.0/24"]
   "ru-central1-b" = ["192.168.20.0/24"]
   "ru-central1-c" = ["192.168.30.0/24"]
  }
}  

variable "private_subnet"{
 type = map
 default = {
   "ru-central1-a" = ["192.168.40.0/24"]
   "ru-central1-b" = ["192.168.50.0/24"]
   "ru-central1-c" = ["192.168.60.0/24"]
  }
} 
